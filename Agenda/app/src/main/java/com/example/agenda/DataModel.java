package com.example.agenda;

import java.util.ArrayList;

public class DataModel {
    private static DataModel instance = new DataModel();
    public ArrayList<Person>listPeople;
    public int index;

    private DataModel(){
        index=0;
        listPeople=new ArrayList<>();
        listPeople.add(new Person("Momo","0752524848","Maryse"));
        listPeople.add(new Person("Tom","0748485252","Bastié"));
    }

    public Person getCurrentPerson(){

        return listPeople.get(index);
    }

    public ArrayList<String> getlistNames(){
        ArrayList<String> names= new ArrayList<>();
        for(Person person:listPeople) {
            names.add(person.getName());
        }
        return names;
    }

    public static DataModel getInstance(){
        return instance;
    }
}
