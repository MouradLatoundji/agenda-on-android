package com.example.agenda;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class List extends AppCompatActivity {

    TextView textViewTitle;
    ListView listViewItem;
    TextView textViewLabel;
    Button buttonAdd;

    private boolean onDelete=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Agenda");
        textViewTitle=findViewById(R.id.textViewTitle);
        listViewItem=findViewById(R.id.listViewItem);
        buttonAdd=findViewById(R.id.buttonAdd);
        textViewLabel=findViewById(R.id.textViewLabel);

        listViewItem.setChoiceMode(listViewItem.CHOICE_MODE_SINGLE);

        /*ArrayAdapter<String> adapter = new ArrayAdapter<>(
                List.this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                DataModel.getInstance().getlistNames()
        );*/


        listViewItem.setAdapter(new ArrayAdapter<>(
                List.this, android.R.layout.simple_list_item_1,
                android.R.id.text1,
                DataModel.getInstance().getlistNames()
        ));
        listViewItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,
                                    int i, long l) {
                DataModel.getInstance().index = i;
                if(!onDelete) {
                    Intent intent = new Intent(List.this, Manage.class);
                    intent.putExtra("index", DataModel.getInstance().index);
                    startActivity(intent);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            List.this);
                    builder.setTitle(android.R.string.dialog_alert_title);
                    builder.setMessage("Do you want to delete this person?");
                    builder.setNegativeButton(android.R.string.no,null);
                    builder.setPositiveButton(android.R.string.yes,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    DataModel.getInstance().listPeople.remove(DataModel.getInstance().index);
                                    //adapter.notifyDataSetChanged();
                                    listViewItem.setAdapter(new ArrayAdapter<>(
                                            List.this, android.R.layout.simple_list_item_1,
                                            android.R.id.text1,
                                            DataModel.getInstance().getlistNames()
                                    ));
                                    Toast.makeText(List.this,"Item deleted",Toast.LENGTH_LONG).show();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        });

       buttonAdd.setOnLongClickListener(new View.OnLongClickListener() {
           @Override
           public boolean onLongClick(View v) {
               if(onDelete) {
                   onDelete=false;
                   buttonAdd.setText("Add an item");
                   textViewLabel.setText("Press hold to delete");
               }
               else {
                   onDelete=true;
                   buttonAdd.setText("Delete an item");
                   textViewLabel.setText("Press hold to add");
               }
               return true;
           }
       });
    }



    public void onClickButtonAdd(View view){
        if(onDelete) {
            Toast.makeText(List.this,"Press hold to add",Toast.LENGTH_LONG).show();
        }
        else {
            Intent intent = new Intent(List.this, Manage.class);
            startActivity(intent);
        }
    }




}